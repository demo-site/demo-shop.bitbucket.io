/**
 * Подключение JS файлов которые начинаются с подчеркивания
 */
//=require ../_blocks/**/_*.js


/* ^^^
 * JQUERY Actions
 * ========================================================================== */
$(function() {

    'use strict';

    /**
     * определение существования элемента на странице
     */
    $.exists = (selector) => $(selector).length > 0;

    /**
     * [^_]*.js - выборка всех файлов, которые не начинаются с подчеркивания
     */
    //=require ../_blocks/**/[^_]*.jquery.js

    //Стилизация инпутов
    $('input, select').styler();

    
    function wimdowResize() {
   
        //Перемещение блока "Поиск" по странице на разнах экранах
        function moveSearchToPage() {
            var searchBlock = $('.search'),
                headerItemForSearch = $('.app-header__search'),
                headerItemForNav = $('.app-header__for-nav'),
                personalAccountBlock = $('.personal-account');

            if (window.matchMedia("(min-width: 1580px)").matches) {
                personalAccountBlock.before(searchBlock);
            }
            else if (window.matchMedia("(max-width: 991px)").matches) {
                searchBlock.appendTo(headerItemForNav);
            }
            else {
                searchBlock.appendTo(headerItemForSearch);
            }
        }

        moveSearchToPage();

        //Перемещение блока "Скидки/Распродажа" в левую колонку под навигацией категорий
        function moveSaleToPage() {
            var saleBlock = $('.sale'),
                categoryNavBlock = $('.category-nav'),
                headerItemForNav = $('.app-header__for-nav');
            
            if (window.matchMedia("(max-width: 1346px)").matches) {
                categoryNavBlock.after(saleBlock);
            }
            else {
                saleBlock.appendTo(headerItemForNav);
            }
        }

        moveSaleToPage();

        //Перемещение "главного меню" и "левой колонки" в мобильное
        function moveNavigationInMobileMenu() {
            var mobileMenuBlock = $('.mobile-menu'),
                asideLeftBlock = $('.app__aside'),
                asideLeftSection = $('.app__aside-section');
            
            if (window.matchMedia("(max-width: 991px)").matches) {
                asideLeftSection.appendTo(mobileMenuBlock);
            }
            else {
                asideLeftSection.appendTo(asideLeftBlock);
            }
            
        }

        moveNavigationInMobileMenu();

        //Перемещение "верхнего меню" в мобильное
        function moveTopNavInMobileMenu() {
            var mobileMenuBlock = $('.main-nav'),
                categoryNavBlock = $('.category-nav'),
                headerBtnBurger = $('.app-header__btn-burger');
            
            if (window.matchMedia("(max-width: 991px)").matches) {
                mobileMenuBlock.insertAfter(categoryNavBlock);
                mobileMenuBlock.addClass('main-nav--mobile')
            }
            else {
                mobileMenuBlock.insertAfter(headerBtnBurger);
                mobileMenuBlock.removeClass('main-nav--mobile')
            }
            
        }

        moveTopNavInMobileMenu();

        //Перемещение "Копирайта" в футере начиная с 744px
        function moveFooterInMobile() {
            var footerItemForCopy = $('.app-footer__item--for-copy'),
                appFooterItems = $('.app-footer__items');
            
            if (window.matchMedia("(max-width: 744px)").matches) {
                footerItemForCopy.appendTo(appFooterItems);
            }
            
        }

        moveFooterInMobile();

        //Перемещение "Телефонов" в шапке начиная с 479px
        function moveContactsInMobile() {
            var contactsMobileItem = $('.contacts-mobile__body'),
                contactsBlock = $('.contacts'),
                headerContactsItem = $('.app-header__contacts');
            
            if (window.matchMedia("(max-width: 479px)").matches) {
                contactsBlock.appendTo(contactsMobileItem);
            }
            else {
                contactsBlock.appendTo(headerContactsItem);
            }

        }

        moveContactsInMobile();

        //Перемещение "Ссылок" в шапке начиная с 479px
        function moveLinksShopInMobile() {
            var contactsMobileItem = $('.contacts-mobile__body'),
                linksShopBlock = $('.links-shop'),
                headerDelivRegItem = $('.app-header__deliv-reg'),
                headerInfoShopItem = $('.app-header__info-shop');
            
            if (window.matchMedia("(max-width: 479px)").matches) {
                linksShopBlock.appendTo(headerDelivRegItem);
            }
            else {
                linksShopBlock.appendTo(headerInfoShopItem);
            }

        }

        moveLinksShopInMobile();

        //Перемещение "Фильтра" на моб. в отдельный блок начиная с 479px
        function moveFilterOtherBlock() {
            var filterMobBlock = $('.filter-mob'),
                filterMobItem = $('.filter-mob__body'),
                filterBlock = $('.filter'),
                asideSection = $('.app__aside-section'),
                headerForFilterMobBtn = $('.app-header__for-filter-mob-btn'),
                filterMobBtn = $('.filter-mob__btn');
            
            if (window.matchMedia("(max-width: 479px)").matches) {
                filterBlock.appendTo(filterMobItem);
            }
            else {
                filterBlock.appendTo(asideSection);
            }

        }

        moveFilterOtherBlock();

    }   // resize

    


    //Бургер
    function useBurger() {
        var btnBurger = $('.app-header__btn-burger'),
            mobileMenuBlock = $('.mobile-menu');

        btnBurger.on('click', function () {
            $('.open-icon-burger').toggleClass('open-active');
            $('.close-icon-burger').toggleClass('close-active');
            // $('body').toggleClass('show-menu-fixed')

            mobileMenuBlock.toggleClass('show-menu');
        })

        $(document).on('click', function(e) {
            if($(e.target).closest('.app-header__bot-line').length){
                return;
            }
            $('.open-icon-burger').removeClass('open-active');
            $('.close-icon-burger').removeClass('close-active');
            $('.mobile-menu').removeClass('show-menu');
            // $('body').removeClass('show-menu-fixed')
        });
    }

    useBurger();

    // Относится к функции moveContactsInMobile()
    $('.contacts-mobile').on('click', function () {
        $('.contacts-mobile__body').toggleClass('show-contacts')
    });

    $(document).on('click', function(e) {
        if($(e.target).closest('.app-header__info-shop').length){
            return;
        }
        $('.contacts-mobile__body').removeClass('show-contacts');
    });

    // Относится к функции moveFilterOtherBlock()
    $('.app-header__for-filter-mob-btn').on('click', function () {
        $(this).toggleClass('open-filter')        
        $('.filter-mob').toggleClass('show-filter');
    })

    $('.filter-mob__btn').on('click', function () {
        
        $('.filter-mob').toggleClass('show-filter');

    })

    $(document).on('click', function(e) {
        if($(e.target).closest('.app-header__for-filter-mob').length){
            return;
        }
        $('.filter-mob').removeClass('show-filter');
        $('.app-header__for-filter-mob-btn').removeClass('open-filter');
    });

    setTimeout(function() {
        $('html').addClass('ready-page');
        
    }, 1500);

    $(window).on('load resize', wimdowResize)

    $(window).trigger('resize');
});
