$('.lot').each(function () {
    var $thisLot = $(this),
        imgElem = $thisLot.find('img'),
        colorElem = $thisLot.find('.lot__color');

    colorElem.on('click', function () {
        var $thisColorItem = $(this),
            indexItem = $thisColorItem.index();
        
        $thisColorItem.addClass('active-color');
        $thisColorItem.siblings().removeClass('active-color');
        

        switch (indexItem) {
            case 0:
                imgElem.attr('src', 'img/lot-img3.jpg')
                break;
            case 1:
                imgElem.attr('src', 'img/lot-img2.jpg')
                break;
            case 2:
                imgElem.attr('src', 'img/lot-img4.jpg')
                break;
            case 3:
                imgElem.attr('src', 'img/lot-img5.jpg')
                break;
            case 4:
                imgElem.attr('src', 'img/lot-img6.png')
                break;
            case 5:
                imgElem.attr('src', 'img/lot-img.jpg')
                break;
        }
    })

})