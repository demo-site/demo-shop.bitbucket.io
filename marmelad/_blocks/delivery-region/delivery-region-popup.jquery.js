$('.delivery-region__name').on('click', function (event) {
    event.preventDefault();
    event.stopPropagation();
    $('.delivery-region-popup').toggleClass('delivery-region-popup--show');
});

$('.delivery-region-popup__close-x').on('click', function (event) {
    $('.delivery-region-popup').removeClass('delivery-region-popup--show');
});

// $(document).on('click', function(e) {
//     if($(e.target).closest('.app-header__item--deliv-reg').length){
//         return;
//     }
//     $('.delivery-region-popup').removeClass('delivery-region-popup--show');
// });
