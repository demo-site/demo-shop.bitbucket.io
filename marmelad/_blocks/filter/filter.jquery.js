$('.input_range_slider').each(function(index, el) {
    var $this = $(this),
        $lower = $this.closest('.range_slider_wrapper').find('.low'),
        $upper = $this.closest('.range_slider_wrapper').find('.hight'),
        inputRangeMax = $upper.data('max'),
        inputRangeMin = $lower.data('min'),
        arr = [inputRangeMin, inputRangeMax];
        
    var slider = $this.noUiSlider({
        start: [$lower.attr('value'), $upper.attr('value')],
        connect: true,
        behaviour: 'drag-tap',
        format: wNumb({
          decimals: 0
        }),
        range: {
          'min': [ arr[0] ],
          'max': [ arr[1] ]
        }
      });

    slider.Link('lower').to($lower);
    slider.Link('upper').to($upper);
});

setTimeout(function () {
    $('.filter__label').each(function () {
        var $this = $(this),
            checkBoxfind = $this.find('.jq-checkbox'),
            checkBoxElFind = $this.find('.jq-checkbox__div');
        
        if ($(window).width() >= 1025) {
            $this.on('mouseenter mouseleave', function () {
                checkBoxfind.toggleClass('hover-check');
                checkBoxElFind.toggleClass('hover-check');
                console.log(checkBoxfind)
            });
        }
    })
}, 1500)

