module.exports = {
  app: {
    lang: 'ru',
    stylus: {
      theme_color: '#3E50B4',
    },
    GA: false, // Google Analytics site's ID
    package: 'ключ перезаписывается значениями из package.json marmelad-сборщика',
    settings: 'ключ перезаписывается значениями из файла настроек settings.marmelad.js',
    storage: 'ключ перезаписывается значениями из файла настроек settings.marmelad.js',
    buildTime: '',
    controls: [
      'default',
      'brand',
      'success',
      'info',
      'warning',
      'danger',
    ],
  },

  crumbs: {
    categoryPage : [
        {
            title : "Главная",
            url  : "#"
        },
        {
            title : "Мужские сумки",
            url  : "#"
        }
    ]
  },

  popularCities: [
    {
      'nameCity':'Москва'
    },
    {
      'nameCity':'Саратов'
    },
    {
      'nameCity':'Ростов-на-дону'
    },
    {
      'nameCity':'Тюмень'
    },
    {
      'nameCity':'Тверь'
    },
    {
      'nameCity':'Самара'
    },
    {
      'nameCity':'Иркутск'
    },
    {
      'nameCity':'Уфа'
    },
    {
      'nameCity':'Екатеринбург'
    },
    {
      'nameCity':'Новосибирск'
    },
    {
      'nameCity':'Санкт-Петербург'
    },
    {
      'nameCity':'Саратов'
    },
    {
      'nameCity':'Ростов-на-дону'
    },
    {
      'nameCity':'Тюмень'
    },
    {
      'nameCity':'Тверь'
    },
    {
      'nameCity':'Самара'
    },
    {
      'nameCity':'Иркутск'
    },
    {
      'nameCity':'Уфа'
    },
    {
      'nameCity':'Екатеринбург'
    },
    {
      'nameCity':'Новосибирск'
    },
    {
      'nameCity':'Красноярск'
    },
    {
      'nameCity':'Саратов'
    },
    {
      'nameCity':'Ростов-на-дону'
    },
    {
      'nameCity':'Тюмень'
    },
    {
      'nameCity':'Тверь'
    },
    {
      'nameCity':'Самара'
    },
    {
      'nameCity':'Иркутск'
    },
    {
      'nameCity':'Уфа'
    },
    {
      'nameCity':'Екатеринбург'
    },
    {
      'nameCity':'Новосибирск'
    }
  ],
  
  pageTitle: 'marmelad',
};
