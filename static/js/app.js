"use strict";

/**
 * Подключение JS файлов которые начинаются с подчеркивания
 */

/**
 * Возвращает функцию, которая не будет срабатывать, пока продолжает вызываться.
 * Она сработает только один раз через N миллисекунд после последнего вызова.
 * Если ей передан аргумент `immediate`, то она будет вызвана один раз сразу после
 * первого запуска.
 */
function debounce(func, wait, immediate) {
  var timeout = null,
      context = null,
      args = null,
      later = null,
      callNow = null;
  return function () {
    context = this;
    args = arguments;

    later = function later() {
      timeout = null;

      if (!immediate) {
        func.apply(context, args);
      }
    };

    callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);

    if (callNow) {
      func.apply(context, args);
    }
  };
} // http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
// requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel
// MIT license


;

(function () {
  var lastTime = 0,
      vendors = ['ms', 'moz', 'webkit', 'o'],
      x,
      currTime,
      timeToCall,
      id;

  for (x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
    window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
    window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
  }

  if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = function (callback) {
      currTime = new Date().getTime();
      timeToCall = Math.max(0, 16 - (currTime - lastTime));
      id = window.setTimeout(function () {
        callback(currTime + timeToCall);
      }, timeToCall);
      lastTime = currTime + timeToCall;
      return id;
    };
  }

  if (!window.cancelAnimationFrame) {
    window.cancelAnimationFrame = function (id) {
      clearTimeout(id);
    };
  }
})();

;

(function () {
  // Test via a getter in the options object to see if the passive property is accessed
  var supportsPassiveOpts = null;

  try {
    supportsPassiveOpts = Object.defineProperty({}, 'passive', {
      get: function get() {
        window.supportsPassive = true;
      }
    });
    window.addEventListener('est', null, supportsPassiveOpts);
  } catch (e) {} // Use our detect's results. passive applied if supported, capture will be false either way.
  //elem.addEventListener('touchstart', fn, supportsPassive ? { passive: true } : false);

})();

function getSVGIconHTML(name, tag, attrs) {
  if (typeof name === 'undefined') {
    console.error('name is required');
    return false;
  }

  if (typeof tag === 'undefined') {
    tag = 'div';
  }

  var classes = 'svg-icon svg-icon--<%= name %>';
  var iconHTML = ['<<%= tag %> <%= classes %>>', '<svg class="svg-icon__link">', '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#<%= name %>"></use>', '</svg>', '</<%= tag %>>'].join('').replace(/<%= classes %>/g, 'class="' + classes + '"').replace(/<%= tag %>/g, tag).replace(/<%= name %>/g, name);
  return iconHTML;
}
/* ^^^
 * JQUERY Actions
 * ========================================================================== */


$(function () {
  'use strict';
  /**
   * определение существования элемента на странице
   */

  $.exists = function (selector) {
    return $(selector).length > 0;
  };
  /**
   * [^_]*.js - выборка всех файлов, которые не начинаются с подчеркивания
   */
  // $('.cart-preview').on('mouseover', function () {
  //     $('.cart-preview__body').toggleClass('cart-preview__body--show');
  // });
  // $(document).on('click', function(e) {
  //     if($(e.target).closest('.cart-preview').length){
  //         return;
  //     }
  //     $('.cart-preview__body').removeClass('cart-preview__body--show');
  // });


  $('.delivery-region__name').on('click', function (event) {
    event.preventDefault();
    event.stopPropagation();
    $('.delivery-region-popup').toggleClass('delivery-region-popup--show');
  });
  $('.delivery-region-popup__close-x').on('click', function (event) {
    $('.delivery-region-popup').removeClass('delivery-region-popup--show');
  }); // $(document).on('click', function(e) {
  //     if($(e.target).closest('.app-header__item--deliv-reg').length){
  //         return;
  //     }
  //     $('.delivery-region-popup').removeClass('delivery-region-popup--show');
  // });

  $('.input_range_slider').each(function (index, el) {
    var $this = $(this),
        $lower = $this.closest('.range_slider_wrapper').find('.low'),
        $upper = $this.closest('.range_slider_wrapper').find('.hight'),
        inputRangeMax = $upper.data('max'),
        inputRangeMin = $lower.data('min'),
        arr = [inputRangeMin, inputRangeMax];
    var slider = $this.noUiSlider({
      start: [$lower.attr('value'), $upper.attr('value')],
      connect: true,
      behaviour: 'drag-tap',
      format: wNumb({
        decimals: 0
      }),
      range: {
        'min': [arr[0]],
        'max': [arr[1]]
      }
    });
    slider.Link('lower').to($lower);
    slider.Link('upper').to($upper);
  });
  setTimeout(function () {
    $('.filter__label').each(function () {
      var $this = $(this),
          checkBoxfind = $this.find('.jq-checkbox'),
          checkBoxElFind = $this.find('.jq-checkbox__div');

      if ($(window).width() >= 1025) {
        $this.on('mouseenter mouseleave', function () {
          checkBoxfind.toggleClass('hover-check');
          checkBoxElFind.toggleClass('hover-check');
          console.log(checkBoxfind);
        });
      }
    });
  }, 1500);
  $('.lot').each(function () {
    var $thisLot = $(this),
        imgElem = $thisLot.find('img'),
        colorElem = $thisLot.find('.lot__color');
    colorElem.on('click', function () {
      var $thisColorItem = $(this),
          indexItem = $thisColorItem.index();
      $thisColorItem.addClass('active-color');
      $thisColorItem.siblings().removeClass('active-color');

      switch (indexItem) {
        case 0:
          imgElem.attr('src', 'img/lot-img3.jpg');
          break;

        case 1:
          imgElem.attr('src', 'img/lot-img2.jpg');
          break;

        case 2:
          imgElem.attr('src', 'img/lot-img4.jpg');
          break;

        case 3:
          imgElem.attr('src', 'img/lot-img5.jpg');
          break;

        case 4:
          imgElem.attr('src', 'img/lot-img6.png');
          break;

        case 5:
          imgElem.attr('src', 'img/lot-img.jpg');
          break;
      }
    });
  });

  (function ($) {
    'use strict';

    var PAGE = $('html, body');
    var pageScroller = $('.page-scroller'),
        pageYOffset = 0,
        inMemory = false,
        inMemoryClass = 'page-scroller--memorized',
        isVisibleClass = 'page-scroller--visible',
        enabledOffset = 60;

    function resetPageScroller() {
      setTimeout(function () {
        if (window.pageYOffset > enabledOffset) {
          pageScroller.addClass(isVisibleClass);
        } else if (!pageScroller.hasClass(inMemoryClass)) {
          pageScroller.removeClass(isVisibleClass);
        }
      }, 150);

      if (!inMemory) {
        pageYOffset = 0;
        pageScroller.removeClass(inMemoryClass);
      }

      inMemory = false;
    }

    if (pageScroller.length > 0) {
      window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
        passive: true
      } : false);
      pageScroller.on('click', function (event) {
        event.preventDefault();
        window.removeEventListener('scroll', resetPageScroller);

        if (window.pageYOffset > 0 && pageYOffset === 0) {
          inMemory = true;
          pageYOffset = window.pageYOffset;
          pageScroller.addClass(inMemoryClass);
          PAGE.stop().animate({
            scrollTop: 0
          }, 500, 'swing', function () {
            window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
              passive: true
            } : false);
          });
        } else {
          pageScroller.removeClass(inMemoryClass);
          PAGE.stop().animate({
            scrollTop: pageYOffset
          }, 500, 'swing', function () {
            pageYOffset = 0;
            window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
              passive: true
            } : false);
          });
        }
      });
    }
  })(jQuery); //Стилизация инпутов


  $('input, select').styler();

  function wimdowResize() {
    //Перемещение блока "Поиск" по странице на разнах экранах
    function moveSearchToPage() {
      var searchBlock = $('.search'),
          headerItemForSearch = $('.app-header__search'),
          headerItemForNav = $('.app-header__for-nav'),
          personalAccountBlock = $('.personal-account');

      if (window.matchMedia("(min-width: 1580px)").matches) {
        personalAccountBlock.before(searchBlock);
      } else if (window.matchMedia("(max-width: 991px)").matches) {
        searchBlock.appendTo(headerItemForNav);
      } else {
        searchBlock.appendTo(headerItemForSearch);
      }
    }

    moveSearchToPage(); //Перемещение блока "Скидки/Распродажа" в левую колонку под навигацией категорий

    function moveSaleToPage() {
      var saleBlock = $('.sale'),
          categoryNavBlock = $('.category-nav'),
          headerItemForNav = $('.app-header__for-nav');

      if (window.matchMedia("(max-width: 1346px)").matches) {
        categoryNavBlock.after(saleBlock);
      } else {
        saleBlock.appendTo(headerItemForNav);
      }
    }

    moveSaleToPage(); //Перемещение "главного меню" и "левой колонки" в мобильное

    function moveNavigationInMobileMenu() {
      var mobileMenuBlock = $('.mobile-menu'),
          asideLeftBlock = $('.app__aside'),
          asideLeftSection = $('.app__aside-section');

      if (window.matchMedia("(max-width: 991px)").matches) {
        asideLeftSection.appendTo(mobileMenuBlock);
      } else {
        asideLeftSection.appendTo(asideLeftBlock);
      }
    }

    moveNavigationInMobileMenu(); //Перемещение "верхнего меню" в мобильное

    function moveTopNavInMobileMenu() {
      var mobileMenuBlock = $('.main-nav'),
          categoryNavBlock = $('.category-nav'),
          headerBtnBurger = $('.app-header__btn-burger');

      if (window.matchMedia("(max-width: 991px)").matches) {
        mobileMenuBlock.insertAfter(categoryNavBlock);
        mobileMenuBlock.addClass('main-nav--mobile');
      } else {
        mobileMenuBlock.insertAfter(headerBtnBurger);
        mobileMenuBlock.removeClass('main-nav--mobile');
      }
    }

    moveTopNavInMobileMenu(); //Перемещение "Копирайта" в футере начиная с 744px

    function moveFooterInMobile() {
      var footerItemForCopy = $('.app-footer__item--for-copy'),
          appFooterItems = $('.app-footer__items');

      if (window.matchMedia("(max-width: 744px)").matches) {
        footerItemForCopy.appendTo(appFooterItems);
      }
    }

    moveFooterInMobile(); //Перемещение "Телефонов" в шапке начиная с 479px

    function moveContactsInMobile() {
      var contactsMobileItem = $('.contacts-mobile__body'),
          contactsBlock = $('.contacts'),
          headerContactsItem = $('.app-header__contacts');

      if (window.matchMedia("(max-width: 479px)").matches) {
        contactsBlock.appendTo(contactsMobileItem);
      } else {
        contactsBlock.appendTo(headerContactsItem);
      }
    }

    moveContactsInMobile(); //Перемещение "Ссылок" в шапке начиная с 479px

    function moveLinksShopInMobile() {
      var contactsMobileItem = $('.contacts-mobile__body'),
          linksShopBlock = $('.links-shop'),
          headerDelivRegItem = $('.app-header__deliv-reg'),
          headerInfoShopItem = $('.app-header__info-shop');

      if (window.matchMedia("(max-width: 479px)").matches) {
        linksShopBlock.appendTo(headerDelivRegItem);
      } else {
        linksShopBlock.appendTo(headerInfoShopItem);
      }
    }

    moveLinksShopInMobile(); //Перемещение "Фильтра" на моб. в отдельный блок начиная с 479px

    function moveFilterOtherBlock() {
      var filterMobBlock = $('.filter-mob'),
          filterMobItem = $('.filter-mob__body'),
          filterBlock = $('.filter'),
          asideSection = $('.app__aside-section'),
          headerForFilterMobBtn = $('.app-header__for-filter-mob-btn'),
          filterMobBtn = $('.filter-mob__btn');

      if (window.matchMedia("(max-width: 479px)").matches) {
        filterBlock.appendTo(filterMobItem);
      } else {
        filterBlock.appendTo(asideSection);
      }
    }

    moveFilterOtherBlock();
  } // resize
  //Бургер


  function useBurger() {
    var btnBurger = $('.app-header__btn-burger'),
        mobileMenuBlock = $('.mobile-menu');
    btnBurger.on('click', function () {
      $('.open-icon-burger').toggleClass('open-active');
      $('.close-icon-burger').toggleClass('close-active'); // $('body').toggleClass('show-menu-fixed')

      mobileMenuBlock.toggleClass('show-menu');
    });
    $(document).on('click', function (e) {
      if ($(e.target).closest('.app-header__bot-line').length) {
        return;
      }

      $('.open-icon-burger').removeClass('open-active');
      $('.close-icon-burger').removeClass('close-active');
      $('.mobile-menu').removeClass('show-menu'); // $('body').removeClass('show-menu-fixed')
    });
  }

  useBurger(); // Относится к функции moveContactsInMobile()

  $('.contacts-mobile').on('click', function () {
    $('.contacts-mobile__body').toggleClass('show-contacts');
  });
  $(document).on('click', function (e) {
    if ($(e.target).closest('.app-header__info-shop').length) {
      return;
    }

    $('.contacts-mobile__body').removeClass('show-contacts');
  }); // Относится к функции moveFilterOtherBlock()

  $('.app-header__for-filter-mob-btn').on('click', function () {
    $(this).toggleClass('open-filter');
    $('.filter-mob').toggleClass('show-filter');
  });
  $('.filter-mob__btn').on('click', function () {
    $('.filter-mob').toggleClass('show-filter');
  });
  $(document).on('click', function (e) {
    if ($(e.target).closest('.app-header__for-filter-mob').length) {
      return;
    }

    $('.filter-mob').removeClass('show-filter');
    $('.app-header__for-filter-mob-btn').removeClass('open-filter');
  });
  setTimeout(function () {
    $('html').addClass('ready-page');
  }, 1500);
  $(window).on('load resize', wimdowResize);
  $(window).trigger('resize');
});